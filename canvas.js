const canvas = document.querySelector("#canvas");
const ctx = canvas.getContext("2d");
const colorPicker = document.querySelector("#brushColor");
const sizePicker = document.querySelector("#brushSize");
const resetButton = document.getElementById("reset");
const undoButton = document.getElementById("undo");
const redoButton = document.getElementById("redo");
const saveButton = document.getElementById("save");
const upload = document.getElementById('upload');
const fontStylePicker = document.getElementById("fontstyle");
const fontSizePicker = document.getElementById("fontSize");

let mouseActive = false;
var mode = "pen";
var lastX;
var lastY;

var mouseX = 0;
var mouseY = 0;
var startingX = 0;
var startingY = 0;
var fontStyle = 'Comic Sans MS';
var fontSize = 20;

let step = -1;
let CanvasList = [];
var inputtext = true;

//handle mouse event
canvas.addEventListener('mousedown', e => {
    mouseActive = true; 
    ctx.beginPath();
    lastX = e.offsetX;
    lastY = e.offsetY;
    startingX = e.offsetX;
    startingY = e.offsetY;
})
canvas.addEventListener('mouseup', e => {
    mouseActive = false;
    if(mode != "text") Update();
    if(mode != "pen"){
        ctx.globalCompositeOperation="source-over";
    }
})
canvas.addEventListener('mouseout', e => {
    mouseActive = false;
    ctx.closePath();
})
canvas.addEventListener('mousemove', drawing);
canvas.addEventListener('click', e => {
    mouseX = e.offsetX;
    mouseY = e.offsetY;
    if(inputtext) texting(mouseX, mouseY);
});
window.addEventListener('load', Init);

//handle toolbox function
var Color = 'black';
var Size = 5;
colorPicker.addEventListener('change', color => {
    Color = color.target.value;
    ctx.strokeStyle = Color;
    ctx.font.fillstyle = Color;
    ctx.fillStyle = Color;
    
})
sizePicker.addEventListener('input', size => {
    const S = size.target.value;
    var sizeShow = document.getElementById("brushSizeShow");
    sizeShow.innerHTML = S;
    ctx.lineWidth = S;
    Size = S;
})
resetButton.addEventListener('click', c => {
    ctx.clearRect(0,0,canvas.clientWidth, canvas.clientHeight);
    Update();
    if(step < CanvasList.length-1)
    {
        CanvasList.length = step+1;   
    } 
})
undoButton.addEventListener('click', undo);
redoButton.addEventListener('click', redo);
saveButton.addEventListener('click', save);
upload.addEventListener('change', e => {
    if(e.target.files)
    {
        let imgfile = e.target.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(imgfile);
        reader.onloadend = function(e){
            var myImg = new Image();
            myImg.src = e.target.result;
            myImg.onload = function(ev){
                ctx.canvas.width = myImg.width;
                ctx.canvas.height = myImg.height;
                ctx.drawImage(myImg, 0, 0);
                Init();
            }
        }
    }
});
fontSizePicker.addEventListener('input', e=>
{
    fontSize = e.target.value;
    ctx.font = fontSize + 'px' + ' ' + fontStyle;
    var sizeShow = document.getElementById("frontSizeShow");
    sizeShow.innerHTML = e.target.value;
})
fontStylePicker.addEventListener('change', e=>
{
    fontStyle = e.target.value;
    ctx.font = fontSize + 'px' + ' ' + fontStyle;
})

function drawing(b){
    let img = new Image();
    img.src = CanvasList[step];
    if(!mouseActive) return;
    ctx.lineCap = "round";  
    var nowX = b.offsetX;
    var nowY = b.offsetY;

    if(mode == "pen")
    {
        ctx.globalCompositeOperation = 'source-over';
        ctx.lineTo(nowX, nowY);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(nowX, nowY);
    }
    else if (mode == "erase"){
        ctx.globalCompositeOperation = 'destination-out';
        ctx.beginPath();
        ctx.arc(nowX, nowY, 10, 0, 2 * Math.PI);
        ctx.fill();

        ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(nowX, nowY);
        ctx.stroke();
        lastX = nowX;
        lastY = nowY;
    }
    else if(mode == "text"){
        ctx.globalCompositeOperation = 'source-over';
        if(step < CanvasList.length-1)
        {
            CanvasList.length = step+1;
        }
    }
    else if(mode == "rec"){
        if(!mouseActive)
            return;
        ctx.beginPath();
        var w = nowX - startingX;
        var h = nowY - startingY;
        ctx.rect(startingX, startingY, w, h);
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0);
        ctx.stroke();
        ctx.closePath();
        if(step < CanvasList.length-1)
        {
            CanvasList.length = step+1;
        }
    }
    else if(mode == "tri"){
        if(!mouseActive) return;
        ctx.beginPath();
        ctx.moveTo(startingX + (nowX - startingX) / 2, startingY);
        ctx.lineTo(nowX, nowY);
        ctx.lineTo(startingX, nowY);
        ctx.lineTo(startingX + (nowX - startingX) / 2, startingY);
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0);
        ctx.stroke();
        ctx.closePath();
        if(step < CanvasList.length-1)
        {
            CanvasList.length = step+1;
        }
    }
    else if(mode == "cir"){
        if(!mouseActive) return;
        ctx.beginPath();
        var r = Math.abs(nowX-startingX)/2; 
        var w = nowX>startingX ? startingX + r: startingX - r;
        var h = nowY>startingY ? startingY + r: startingY - r;
        ctx.arc(w, h, r, 0, 2*Math.PI, false);
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img, 0, 0);
        ctx.stroke();
        ctx.closePath();
        if(step < CanvasList.length-1)
        {
            CanvasList.length = step+1;
        }
    }
}
var tx = 0;
var ty = 0;
function texting(x, y){
    tx = x;
    ty = y;
    if(mode != 'text') return;
    var t = document.createElement('input');
    t.setAttribute('type','text');
    t.style.border = '1px solid #24358A';
    t.style.position = 'absolute';
    t.style.left = x + 350 + 'px';
    t.style.top = y + 80 + 'px';
    document.body.appendChild(t);
    if(t.value == "") inputtext = false;
    t.onkeydown = finishtext;
}
function finishtext(e)
{
    if(e.key == 'Enter')
    {
        ctx.fillText(this.value, tx, ty);
        this.remove();
        inputtext = true;
        if(step < CanvasList.length-1)
        {
            CanvasList.length = step+1;
        }
        Update();
    }
}

function Init()
{
    Update();
    canvas.style.cursor = "url('./pic/brush.png'), auto";
    ctx.lineWidth = Size;
    ctx.strokeStyle = Color;
    ctx.font.fillstyle = Color;
    ctx.fillStyle = Color;
    ctx.font = '20px Comic Sans MS';
}

function Update()
{
    step++;
    if(step < CanvasList.length-1)
    {
        CanvasList.length = step+1;
    }
    CanvasList.push(canvas.toDataURL())
    console.log('step:' + step+' '+'CanvasList:'+ CanvasList.length);

}
function undo() {
    if (step > 0) {
        step--;
        let nextcanvas = new Image(); 
        nextcanvas.src = CanvasList[step];
        nextcanvas.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(nextcanvas, 0, 0);
        }
        console.log('undo5: step:' + step + ' '+'CanvasList:'+ CanvasList.length);
        //console.log(CanvasList)
    }

}
function redo(){
    if(step < CanvasList.length-1)
    {
        step++;
        let nextcanvas = new Image(); 
        nextcanvas.src = CanvasList[step]; 
        nextcanvas.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(nextcanvas, 0, 0);
        }
        console.log('redo: step:' + step+' '+'CanvasList:'+ CanvasList.length);
    }

}
function save(){
    var img = document.createElement('a');
    img.download = 'picture.png';
    img.href = canvas.toDataURL()
    img.click();
    img.delete;
}
function Upload(e){
}


function pOnclick()
{
    mode = "pen";
    canvas.style.cursor = "url('./pic/brush.png'), auto";
    console.log(canvas.style.cursor);
}
function eOnclick()
{
    mode = "erase";
    canvas.style.cursor = "url('./pic/eraser.png'), auto";
}
function tOnclick()
{
    mode = "text";
    canvas.style.cursor = "url('./pic/text.png'), auto";
}
function rOnclick()
{
    mode = "rec";
    canvas.style.cursor = "url('./pic/rec.png'), auto";
}
function trOnclick()
{
    mode = "tri";
    canvas.style.cursor = "url('./pic/tri.png'), auto";
}
function cOnclick()
{
    mode = "cir";
    canvas.style.cursor = "url('./pic/cir.png'), auto";
}

//for