# Software Studio 2021 Spring

## Assignment 01 Web Canvas

### Scoring

| **Basic components** | **Score** | **Check** |
| :------------------- | :-------: | :-------: |
| Basic control tools  |    30%    |     Y     |
| Text input           |    10%    |     Y     |
| Cursor icon          |    10%    |     Y     |
| Refresh button       |    10%    |     Y     |

| **Advanced tools**     | **Score** | **Check** |
| :--------------------- | :-------: | :-------: |
| Different brush shapes |    15%    |     Y     |
| Un/Re-do button        |    10%    |     Y     |
| Image tool             |    5%     |     Y     |
| Download               |    5%     |     Y     |

| **Other useful widgets** | **Score** | **Check** |
| :----------------------- | :-------: | :-------: |
| Name of widgets          |   1~5%    |     N     |

---

### How to use

    Just click the button below to change the tool.

    From left to right is pen/eraser/color picker/brush size/rectangle/triangle/circle/text/font picker/font size/reset/undo/redo/download/upload picture.

### Function description

    important variable:
        mode:紀錄現在使用的工具模式
        mouseActive:記錄目前滑鼠使用的狀態

    mouse event:
    1.'mouse down':紀錄滑鼠按下的第一個位置，並開始一個path（用於pen/eraser），將變數mouseActive設為true，即滑鼠開始拖曳。
    2.'mouse move':對應drawing的function
    3.'mouse up':將變數mouseActive設為true，即滑鼠結束拖曳。值得注意的是除了在eraser的mode之外，新增上去的線條都應該是覆蓋的(source-over)

    Event Listener:
    1.colorPicker:因為color picker的type是input color，所以當input被change時表示顏色被更改了，此時就更新畫筆、字體的顏色
    2.sizePicker、fontSizePicker:當input range被更改時表示畫筆粗細、字體大小需要更改
    3.resetButton:當reset被click時清空整個畫布

    Onclick:
        當各自button被click時根據對應的Onclick Function去變更mode、Cursor樣式

    Important Function:
    1.Undo Redo:這個部份我是利用一個陣列CanvasList去存每個更新過的圖片，用變數step去記錄目前Undo或redo到哪個位置了
    每次Undo或Redo就先清空原本的畫布並將對應step的List中存的Canvas重新畫上去，而記錄每個步驟的Canvas的Function是Update，每次畫了新的東西就會Update一次，還有每次重新載入頁面的時候也會先Update一次，將空白的畫布存起來

    2.Save: 將目前的Canvas利用toDataURL做成連結並下載下來

    3.upload:upload是input file的type，所以當上傳好檔案(change)後就將檔案讀成URL，並轉成image，讀取完後就draw上去畫布並調整畫布大小。

    4.text:Text的部分我是新增一個input text的element並appendChild在點擊的位置，然後開始偵測onKeyDown的event並叫finishtext的function

    當偵測到enter被按下後就把文字draw上去，然後把input的element移除掉。

    5.Different brush:圓形、正方形、三角形的實作方法類似，都是每次Move都畫一次圖形，但在畫圖形之前會先將上一步的東西做類似Undo的動作一次直到move結束。

### Gitlab page link

    https://108062123.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    辛苦助教ㄌ！
